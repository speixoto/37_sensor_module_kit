import RPi.GPIO as GPIO
import time
import random

class RGB(object):
    def __init__(self, rgb_pins= [3, 5, 7], frequency=50):
        GPIO.setmode(GPIO.BOARD)
        self.rgb_pins = rgb_pins
        self.frequency = frequency
        GPIO.setup(self.rgb_pins, GPIO.OUT)
        self.on()

    def __del__(self):
        GPIO.cleanup()

    def on(self):
        self.p_r = GPIO.PWM(self.rgb_pins[0], self.frequency)
        self.p_g = GPIO.PWM(self.rgb_pins[1], self.frequency)
        self.p_b = GPIO.PWM(self.rgb_pins[2], self.frequency)
        self.p_r.start(0)
        self.p_g.start(0)
        self.p_b.start(0)

    def set(self, red, green, blue):
        self.p_r.ChangeDutyCycle(red * 100.0 / 255.0)
        self.p_g.ChangeDutyCycle(green * 100.0 / 255.0)
        self.p_b.ChangeDutyCycle(blue * 100.0 / 255.0)

    def random(self):
        self.set(random.randint(0,255), random.randint(0,255), random.randint(0,255))

    def off(self):
        GPIO.output(self.rgb_pins, GPIO.LOW)
        self.p_r.stop()
        self.p_g.stop()
        self.p_b.stop()

if __name__ == "__main__":
  rgb = RGB()
  print("Scale of blue")
  rgb.set(102,204,255)
  time.sleep(10)
  rgb.set(51,153,255)
  time.sleep(10)
  rgb.set(0,102,255)
  time.sleep(10)
  rgb.set(102,0,102)
  time.sleep(10)
  rgb.set(0,0,255)
  time.sleep(10)
  rgb.set(0,0,204)



  

