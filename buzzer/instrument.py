import RPi.GPIO as GPIO
import time

class Instrument(object):
 def __init__(self, buzzer_pin=11, tempo=0.4, rgb=None):
  GPIO.setmode(GPIO.BOARD)  
  self.buzzer_pin = buzzer_pin
  self.tempo = tempo
  GPIO.setup(self.buzzer_pin, GPIO.OUT)
  self.rgb = rgb

 def __del__(self):
  GPIO.cleanup()

 def get_frequency(self, note, octave):
   if note == 'do' or note == 'C':
     return [16.35, 32.70, 65.41, 130.81, 261.63, 523,25, 1046,50, 2093.0, 4186.01][octave]
   elif note == 'do#' or note == 'reb' or note == 'C#' or note == 'Db':
     return [17.32, 34.65, 69.30, 138.59, 277.18, 554.37, 1108.73, 2217.46, 4434.92][octave]
   elif note == 're' or note == 'D':
     return [18.35, 36.71, 73.42, 146.83, 293.66, 587.33, 1174.66, 2349.32, 4698.63][octave]
   elif note == 're#' or note == 'mib' or note == 'D#' or note == 'Eb':
     return [19.45, 38.89, 77.78, 155.56, 311.13, 622.25, 1244.51, 2489.02, 4978.03][octave]
   elif note == 'mi' or note == 'E':
     return [20.60, 41.20, 82.41, 164.81, 329.63, 659.25, 1318.51, 2637.02, 5274.04][octave]
   elif note == 'fa' or note == 'F':
     return [21.83, 43.65, 87.31, 174.61, 349.23, 698.46, 1396.91, 2793.83, 5587.65][octave]
   elif note == 'fa#' or note == 'solb' or note == 'F#' or note == 'Gb':
     return [23.12, 46.25, 92.50, 185.00, 369.99, 739.99, 1479.98, 2959.96, 5919.91][octave]
   elif note == 'sol' or note == 'G':
     return [24.50, 49.00, 98.00, 196.00, 392.00, 783.99, 1567.98, 3135.96, 6271.93][octave]
   elif note == 'sol#' or note == 'lab' or note == 'G#' or note == 'Ab':
     return [25.96, 51.91, 103.83, 207.65, 415.30, 830.61, 1661.22, 3322.44, 6644.88][octave]
   elif note == 'la' or note == 'A':
     return [27.50, 55.00, 110.00, 220.00, 440.00, 880.00, 1760.00, 3520.00, 7040.00][octave]
   elif note == 'la#' or note == 'sib' or note == 'A#' or note == 'Bb':
     return [29.14, 58.27, 116.54, 233.08, 466.16, 932.33, 1864.66, 3729.31, 7458.62][octave]
   elif note == 'si' or note == 'B':
     return [30.87, 61.74, 123.47, 246.94, 493.88, 987.77, 1975.53, 3951.07, 7902.13][octave]

 def buzz(self, note, octave, duration):
   frequency = self.get_frequency(note, octave)
   p = GPIO.PWM(self.buzzer_pin, frequency)
   p.start(50.0)
   time.sleep(duration * self.tempo)
   p.stop()
   time.sleep(0.05)

 def play(self, partiture):
   for note, octave, duration in partiture:
     if self.rgb:
       self.rgb.random()
     self.buzz(note, octave, duration)

if __name__ == "__main__":
  instrument = Instrument()
  instrument.play([
       ('G', 3, 0.5),
       ('G', 3, 0.5),
       ('A', 3, 1),
       ('G', 3, 1),
       ('C', 4, 1),
       ('B', 3, 2),
       ('G', 3, 0.5),
       ('G', 3, 0.5),
       ('A', 3, 1),
       ('G', 3, 1),
       ('D', 4, 1),
       ('C', 4, 2),
       ('G', 3, 0.5),
       ('G', 3, 0.5),
       ('G', 4, 1),
       ('E', 4, 1),
       ('C', 4, 1),
       ('B', 3, 1),
       ('A', 3, 1),
       ('F', 4, 0.5),
       ('F', 4, 0.5),
       ('E', 4, 1),
       ('C', 4, 1),
       ('D', 4, 1),
       ('C', 4, 2)
  ])
