import RPi.GPIO as GPIO
import time
import random
from buzzer.instrument import Instrument
from led.rgb import RGB

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

led = RGB()

instrument = Instrument(rgb=led)

musics = [
    [
       ('sol', 3, 0.5),
       ('sol', 3, 0.5),
       ('la', 3, 1),
       ('sol', 3, 1),
       ('do', 4, 1),
       ('si', 3, 2),
       ('sol', 3, 0.5),
       ('sol', 3, 0.5),
       ('la', 3, 1),
       ('sol', 3, 1),
       ('re', 4, 1),
       ('do', 4, 2),
       ('sol', 3, 0.5),
       ('sol', 3, 0.5),
       ('sol', 4, 1),
       ('mi', 4, 1),
       ('do', 4, 1),
       ('si', 3, 1),
       ('la', 3, 1),
       ('fa', 4, 0.5),
       ('fa', 4, 0.5),
       ('mi', 4, 1),
       ('do', 4, 1),
       ('re', 4, 1),
       ('do', 4, 2)
    ],
    [
       ('do', 4, 1),
       ('do', 4, 1),
       ('do', 4, 1),
       ('re', 4, 1),
       ('do', 4, 1),
       ('do', 4, 1),
       ('sol', 3, 2),
       ('la', 3, 1),
       ('sol', 3, 1),
       ('la', 3, 1),
       ('si', 3, 1),
       ('do', 4, 2),
       ('do', 4, 2),
       ('do', 4, 1),
       ('do', 4, 1),
       ('do', 4, 1),
       ('re', 4, 1),
       ('do', 4, 1),
       ('do', 4, 1),
       ('sol', 3, 2),
       ('la', 3, 1),
       ('sol', 3, 1),
       ('la', 3, 1),
       ('si', 3, 1),
       ('do', 4, 2),
       ('do', 4, 2),
    ],
    [
       ('do', 4, 2),
       ('do', 4, 1),
       ('do', 4, 1),
       ('re', 4, 1),
       ('mi', 4, 1),
       ('sol', 4, 2),
       ('mi', 4, 1),
       ('do', 4, 2),
       ('do', 4, 1),
       ('re', 4, 2),
       ('mi', 4, 1),
       ('re', 4, 2),
       ('mi', 4, 1),
       ('re', 4, 2),
       ('si', 3, 1),
       ('sol', 3, 3),
       ('do', 4, 2),
       ('do', 4, 1),
       ('do', 4, 1),
       ('re', 4, 1),
       ('mi', 4, 1),
       ('sol', 4, 2),
       ('mi', 4, 1),
       ('do', 4, 2),
       ('do', 4, 1),
       ('re', 4, 2),
       ('re', 4, 1),
       ('sol', 3, 1),
       ('la', 3, 1),
       ('si', 3, 1),
       ('do', 4, 3),
       ('do', 4, 3),
    ],
    [
       ('do', 4, 1),
       ('re', 4, 1),
       ('mi', 4, 2),
       ('do', 4, 1),
       ('re', 4, 1),
       ('mi', 4, 2),
       ('re', 4, 1),
       ('do', 4, 1),
       ('re', 4, 1),
       ('mi', 4, 1),
       ('do', 4, 2),
       ('do', 4, 2),
    ]
]

try:
    count = 0
    while(True):
        time.sleep(0.5)
        instrument.play(musics[count % 4])
        count += 1
except KeyboardInterrupt:
    print("Finished")
